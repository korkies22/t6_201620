package taller.estructuras;


public class TablaLlavesRepetidas<K extends Comparable<K>,V extends Comparable<V>> {

	//TODO Una enumeracioón que representa los tipos de colisiones que se pueden manejar
	public enum Colisiones
	{
		LINEAR_PROBING,
		SEPARATE_CHAINING
	}

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	//TODO: Agruegue la estructura que va a soportar la tabla.
	private Lista<K,V>[] keys;      // the keys

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	/**
	 * La ocupación actual de la tabla, numero de posiciones en el arreglo que están ocupadas.
	 */
	private int ocupacion;

	//Constructores

	@SuppressWarnings("unchecked")
	public TablaLlavesRepetidas(){
		//TODO: Inicialice la tabla con los valores que considere prudentes para una ejecución normal
		this(4000037,0.5f );
	}

	@SuppressWarnings("unchecked")
	public TablaLlavesRepetidas(int pCapacidad, float pFactorCargaMax) {
		//TODO: Inicialice la tabla con los valores dados por parametro
		capacidad= pCapacidad;
		factorCargaMax=pFactorCargaMax;
		factorCarga=0;
		ocupacion=0;
		keys = new Lista[capacidad];
	}

	public void put(K llave, V valor){

		// double table size if 50% full
		if (factorCarga > factorCargaMax) resize(2*capacidad);
		int i;
		for (i = hash(llave); keys[i] != null; i = (i + 1) % capacidad) {
			if (keys[i].darLlave().equals(llave))
			{
				keys[i].put(llave, valor);
				count++;
				return;
			}
		}
		keys[i]= new Lista<K,V>(llave);
		keys[i].put(llave, valor);
		count++;
		ocupacion++;
		factorCarga=(float)ocupacion/capacidad;
	}

	public void put(Lista l)
	{
		// double table size if 50% full
		if (factorCarga > factorCargaMax) resize(2*capacidad);
		int i;
		for (i = hash((K) l.darLlave()); keys[i] != null; i = (i + 1) % capacidad) 
		{

		}
		keys[i]= l;
		count+=l.size();
		ocupacion++;
		factorCarga=(float)ocupacion/capacidad;
	}

	public int darCapacidad()
	{
		return capacidad;
	}

	public int darNumElementos()
	{
		return count;
	}

	public int darOcupacion()
	{
		return ocupacion;
	}

	public Lista<K, V> get(K llave){
		//TODO: Busque y retorne el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		for (int i = hash(llave); keys[i] != null; i = (i + 1) % capacidad)
			if (keys[i].darLlave().equals(llave))
				return keys[i];
		return null;
	}

	public Lista delete(K llave){
		Lista retorno= null;
		if ( get(llave) == null) return null;

		// find position i of key
		int i = hash(llave);
		while (!llave.equals(keys[i].darLlave())) {
			i = (i + 1) % capacidad;
		}

		// delete key and associated value
		retorno= keys[i];
		keys[i] = null;

		// rehash all keys in same cluster
		i = (i + 1) % capacidad;
		while (keys[i] != null) {
			// delete keys[i] an vals[i] and reinsert
			Lista  keyToRehash = keys[i];
			keys[i] = null;
			count-=keyToRehash.size();
			ocupacion--;
			put(keyToRehash);
			i = (i + 1) % capacidad;
		}
		count-=retorno.size();
		ocupacion--;
		if (ocupacion > 0 && ocupacion <= capacidad/8) resize(capacidad/2);
		return retorno;
	}

	//Hash
	private int hash(K llave)
	{
		return Math.abs(((llave.hashCode()/11)-2)%capacidad);
	}

	//TODO: Permita que la tabla sea dinamica
	private void resize(int pCapacidad)
	{
		TablaLlavesRepetidas<K, V> temp = new TablaLlavesRepetidas<K, V>(pCapacidad, factorCargaMax);
		for (int i = 0; i < capacidad; i++) {
			if (keys[i] != null) {
				temp.put(keys[i]);
			}
		}
		keys = temp.keys;
		capacidad  = temp.capacidad;
		factorCarga=(float)ocupacion/capacidad;
	}

}