package taller.interfaz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;

import taller.estructuras.Lista;
import taller.estructuras.TablaHashLinearProbing;
import taller.estructuras.TablaLlavesRepetidas;
import taller.mundo.Ciudadano;
import taller.mundo.UbicacionGeografica;


public class Main {

	private static String rutaEntrada = "./data/ciudadLondres2.csv";

	public static void main(String[] args) {
		//Cargar registros
		System.out.println("Bienvenido al directorio de emergencias por colicsiones de la ciudad de Londres");
		System.out.println("Espere un momento mientras cargamos la información...");
		System.out.println("Esto puede tardar unos minutos...");
		TablaHashLinearProbing<Integer, Ciudadano> tablaNumero=null;
		TablaLlavesRepetidas<String, Ciudadano> tablaNombre=null;
		TablaHashLinearProbing<UbicacionGeografica, Ciudadano> tablaPosicion=null;
		String[] datos= null;
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaEntrada)));
			String entrada = br.readLine();

			//TODO: Inicialice el directorio t
			tablaNumero= new TablaHashLinearProbing<Integer, Ciudadano>();
			tablaNombre= new TablaLlavesRepetidas<String, Ciudadano>();
			tablaPosicion= new TablaHashLinearProbing<>();
			int i = 0;
			entrada = br.readLine();
			while (entrada != null){
				datos = entrada.split(",");
				//TODO: Agrege los datos al directorio de emergencias
				//Recuerde revisar en el enunciado la estructura de la información
				Ciudadano act=new Ciudadano(datos[0], datos[1], Integer.parseInt(datos[2]), Float.parseFloat(datos[3]), Float.parseFloat(datos[4]));
				tablaNumero.put(Integer.parseInt(datos[2]), act );
				tablaNombre.put(datos[0], act);
				tablaPosicion.put(new UbicacionGeografica(Float.parseFloat(datos[3]), Float.parseFloat(datos[4])), act);
				++i;
				if (i%50000 == 0)
					System.out.println(i+" entradas...");

				entrada = br.readLine();
			}
			System.out.println(i+" entradas cargadas en total");
			br.close();
		} catch (Exception e) {
			System.out.println(datos);
			e.printStackTrace();
		}

		System.out.println("Directorio de emergencias por colisiones de Londres v1.0");

		boolean seguir = true;

		while (seguir)
			try {
				System.out.println("Bienvenido, seleccione alguna opcion del menú a continuación:");
				System.out.println("1: Agregar ciudadano a la lista de emergencia");
				System.out.println("2: Buscar ciudadano por número de contacto del acudiante");
				System.out.println("3: Buscar ciudadano por nombre");
				System.out.println("4: Buscar ciudadano por su localización actual");
				System.out.println("Exit: Salir de la aplicación");

				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				String in = br.readLine();
				switch (in) {
				case "1":
					//TODO: Implemente el requerimiento 1.
					//Agregar ciudadano a la lista de emergencia
					System.out.println("Escriba el nombre");
					String nombre= br.readLine();
					System.out.println("Escriba el apellido");
					String apellido= br.readLine();
					System.out.println("Escriba el numero de telefono");
					String numeroString= br.readLine();
					int num1= Integer.parseInt(numeroString);
					System.out.println("Escriba la latitud");
					String latString= br.readLine();
					float lat= Float.parseFloat(latString);
					System.out.println("Escriba la longitud");
					String lonString= br.readLine();
					float lon= Float.parseFloat(lonString);
					long startTime = System.currentTimeMillis();
					Ciudadano act= new Ciudadano(nombre, apellido, num1, lat, lon);
					tablaNumero.put(num1, act);
					tablaNombre.put(nombre, act);
					tablaPosicion.put(new UbicacionGeografica(lat, lon), act);
					long stopTime = System.currentTimeMillis();
					long elapsedTime = stopTime - startTime;
					System.out.println("tiempo del algoritmo: "+elapsedTime);
					break;
				case "2":
					//TODO: Implemente el requerimiento 2
					//Buscar un ciudadano por el número de teléfono de su acudiente
					System.out.println("Escriba el numero a buscar");
					String numeroString1= br.readLine();
					int num2= Integer.parseInt(numeroString1);
					long startTime1 = System.currentTimeMillis();
					Ciudadano actual=tablaNumero.get(num2);
					System.out.println("nombre completo: "+ actual.darNombre() +" "+ actual.darApellido());
					System.out.println("Latitud: "+ actual.darLatitud());
					System.out.println("Longitud: "+ actual.darLongitud());
					long stopTime1 = System.currentTimeMillis();
					long elapsedTime1 = stopTime1 - startTime1;
					System.out.println("tiempo del algoritmo"+elapsedTime1);
					break;
				case "3":
					//TODO: Implemente el requerimiento 3
					//Buscar ciudadano por apellido/nombre
					System.out.println("Escriba el nombre del ciudadano a buscar");
					String nombre1 = br.readLine();
					Lista listaAct=tablaNombre.get(nombre1);
					Iterator<Ciudadano> i= listaAct.iterator();
					while(i.hasNext())
					{
						Ciudadano act3= i.next();
						System.out.println("Nombre: "+ act3.darNombre());
						System.out.println("Apellido: " + act3.darApellido());
						System.out.println("Número: " + act3.darNumero());
						System.out.println("Latitud: "+ act3.darLatitud());
						System.out.println("Longitud: "+ act3.darLongitud());
					}
					break;
				case "4":
					//TODO: Implemente el requerimiento 4
					//Buscar ciudadano por su locaclización actual
					System.out.println("Escriba la latitud a buscar");
					String latString1= br.readLine();
					float lat1= Float.parseFloat(latString1);
					System.out.println("Escriba la longitud a buscar");
					String lonString1= br.readLine();
					float lon1= Float.parseFloat(lonString1);
					Ciudadano actualPos=tablaPosicion.get(new UbicacionGeografica(lat1, lon1));
					System.out.println(lat1);
					System.out.println(lon1);
					System.out.println("nombre completo: "+ actualPos.darNombre() +" "+ actualPos.darApellido());
					break;
				case "Exit":
					System.out.println("Cerrando directorio...");
					seguir = false;
					break;

				default:
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}


	}

}
