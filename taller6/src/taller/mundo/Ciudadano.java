package taller.mundo;

public class Ciudadano implements Comparable<Ciudadano>
{
	private String nombre;

	private String apellido;

	private int numero;

	private float latitud;
	
	private float longitud;
	public Ciudadano(String pNombre, String pApellido, int pNumero, float pLatitud, float pLongitud)
	{
		nombre= pNombre;
		apellido=pApellido;
		numero=pNumero;
		latitud=pLatitud;
		longitud=pLongitud;
	}
	
	public String darNombre()
	{
		return nombre;
	}
	
	public String darApellido()
	{
		return apellido;
	}
	
	public int darNumero()
	{
		return numero;
	}
	
	public float darLatitud()
	{
		return latitud;
	}
	
	public float darLongitud()
	{
		return longitud;
	}

	@Override
	public int compareTo(Ciudadano o) 
	{
		int comparacion= nombre.compareTo(o.nombre);
		if(nombre.compareTo(o.nombre)==0)
			return apellido.compareTo(o.apellido);
		return comparacion;
	}
}
