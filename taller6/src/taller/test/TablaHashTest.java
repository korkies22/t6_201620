package taller.test;

import java.util.Comparator;
import java.util.Iterator;

import junit.framework.TestCase;
import taller.estructuras.TablaHashLinearProbing;
import taller.mundo.Ciudadano;
import taller.mundo.UbicacionGeografica;

public class TablaHashTest extends TestCase {
	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	 * Clase donde se harán las pruebas.
	 */
	private TablaHashLinearProbing<String, Ciudadano> tabla1;
	
	private TablaHashLinearProbing<Integer, Ciudadano> tabla2;
	
	private TablaHashLinearProbing<UbicacionGeografica, Ciudadano> tabla3;

	// -----------------------------------------------------------------
	// Métodos
	// -----------------------------------------------------------------

	/**
	 * Escenario 1: Construye una nueva tabla con llaves de Strings, capacidad 5 y factor de carga máxima 0.5
	 */
	private void setupEscenario1() 
	{
		tabla1 = new TablaHashLinearProbing<String, Ciudadano>(5, 0.5f);
	}

	/**
	 * Escenario 2: Construye una nueva tabla con llaves de enteros, capacidad 5 y factor de carga máxima 0.5
	 */
	private void setupEscenario2() 
	{
		tabla2 = new TablaHashLinearProbing<Integer, Ciudadano>(5, 0.5f);
	}
	
	private void setupEscenario3()
	{
		tabla3 = new TablaHashLinearProbing<UbicacionGeografica, Ciudadano>(5, 0.5f);
	}

	/**
	 * Prueba 1: Se encarga de verificar el método agregar.<br>
	 * <b> Métodos a probar:</b> <br>
	 * put<br>
	 * get<br>
	 * <b> Casos de prueba: </b><br>
	 * 1. La tabla está vacía. <br>
	 * 2. Se agregan elementos
	 */
	public void testAgregar() {
		setupEscenario1();
		assertEquals("No debería encontrar nada", null , tabla1.get("a"));
		tabla1.put("a", new Ciudadano("a", "b", 1,0,0));
		tabla1.put("c", new Ciudadano("c", "d", 2,0,0));
		assertEquals("El tamaño de la lista debería ser 2", 2, tabla1.darNumElementos());
		assertNotNull("debería encontrar el objeto", tabla1.get("a"));
	
		setupEscenario2();
		assertEquals("No deberían haber elementos", 0, tabla2.darNumElementos());
		tabla2.put(1, new Ciudadano("a", "b", 1,0,0));
		tabla2.put(2, new Ciudadano("c", "d", 2,0,0));
		assertEquals("La capacidad debería seguir siendo 5", 5, tabla2.darCapacidad());
		assertEquals("No debería encontrar nada", null , tabla2.get(3));
		tabla2.put(3, new Ciudadano("c", "d", 3,0,0));
		tabla2.put(4, new Ciudadano("e", "f", 4,0,0));
		assertEquals("La capacidad debería haber aumentado al doble", 10, tabla2.darCapacidad());
		tabla2.put(5, new Ciudadano("g", "h", 5,0,0));
		assertEquals("El numero de elementos debería aumentar", 5, tabla2.darNumElementos());
		assertNotNull("debería encontrar el objeto", tabla2.get(3));
	}

	/**
	 * Prueba 1: Se encarga de verificar el método delete de la clase.<br>
	 * <b> Métodos a probar:</b> <br>
	 * delete<br>
	 * <b> Casos de prueba: </b><br>
	 * 1. Se agregan y se quitan elementos.
	 */
	public void testEliminar() 
	{
		setupEscenario1();
		tabla1.put("a", new Ciudadano("a", "b", 1,0,0));
		tabla1.put("c", new Ciudadano("c", "b", 1,0,0));
		tabla1.put("e", new Ciudadano("e", "f", 3,0,0));
		tabla1.put("g", new Ciudadano("g", "h", 4,0,0));
		tabla1.put("i", new Ciudadano("i", "j", 5,0,0));
		tabla1.put("k", new Ciudadano("k", "l", 6,0,0));
		assertEquals("El numero de elementos debería ser 6", 6, tabla1.darNumElementos());
		assertNotNull("Debería encontrar el objeto", tabla1.delete("a"));
		assertNull("No debería encontrar el objeto", tabla1.delete("a"));
		assertEquals("El numero de elementos debería disminuir", 5, tabla1.darNumElementos());
		assertNotNull("Debería encontrar el objeto", tabla1.delete("k"));
		assertNotNull("Debería encontrar el objeto", tabla1.delete("i"));
		assertNotNull("Debería encontrar el objeto", tabla1.delete("g"));
		assertEquals("El numero de elementos debería disminuir", 2, tabla1.darNumElementos());
		assertNull("No debería encontrar el objeto", tabla1.delete("i"));
		assertNotNull("Debería encontrar el objeto", tabla1.delete("c"));
		assertEquals("La capacidad debería disminuir", 5, tabla1.darCapacidad());
	
		setupEscenario2();
		tabla2.put(1, new Ciudadano("a", "b", 1,0,0));
		tabla2.put(2, new Ciudadano("a", "b", 2,0,0));
		tabla2.put(3, new Ciudadano("e", "f", 3,0,0));
		tabla2.put(4, new Ciudadano("g", "h", 4,0,0));
		tabla2.put(5, new Ciudadano("i", "j", 5,0,0));
		tabla2.put(6, new Ciudadano("k", "l", 6,0,0));
		assertEquals("El numero de elementos debería ser 6", 6, tabla2.darNumElementos());
		assertNotNull("Debería encontrar el objeto", tabla2.delete(1));
		assertNull("No debería encontrar el objeto", tabla2.delete(1));
		assertEquals("El numero de elementos debería disminuir", 5, tabla2.darNumElementos());
		assertNotNull("Debería encontrar el objeto", tabla2.delete(6));
		assertNotNull("Debería encontrar el objeto", tabla2.delete(3));
		assertNotNull("Debería encontrar el objeto", tabla2.delete(4));
		assertEquals("El numero de elementos debería disminuir", 2, tabla2.darNumElementos());
		assertNull("No debería encontrar el objeto", tabla2.delete(3));
		assertNotNull("Debería encontrar el objeto", tabla2.delete(2));
		assertEquals("La capacidad debería disminuir", 5, tabla2.darCapacidad());
	}
	
	public void testAgregarPorUbicacion()
	{
		setupEscenario3();
		
		assertEquals("No deberían haber elementos", 0, tabla3.darNumElementos());
		tabla3.put(new UbicacionGeografica(0, 0), new Ciudadano("a", "b", 1,0,0));
		tabla3.put(new UbicacionGeografica(1, 0), new Ciudadano("c", "d", 1,1,0));
		assertEquals("La capacidad debería seguir siendo 5", 5, tabla3.darCapacidad());
		assertEquals("No debería encontrar nada", null , tabla3.get(new UbicacionGeografica(0, 1)));
		tabla3.put(new UbicacionGeografica(0, 1), new Ciudadano("e", "d", 1,0,1));
		tabla3.put(new UbicacionGeografica(0.1f, -0.5f), new Ciudadano("f", "d", 1,0.1f,-0.5f));
		assertEquals("La capacidad debería haber aumentado al doble", 10, tabla3.darCapacidad());
		tabla3.put(new UbicacionGeografica(-1, (float) -0.1), new Ciudadano("f", "d", 1,-1,-0.1f));
		assertEquals("El numero de elementos debería aumentar", 5, tabla3.darNumElementos());
		assertNotNull("debería encontrar el objeto", tabla3.get(new UbicacionGeografica(1f, 0f)));
	}
}