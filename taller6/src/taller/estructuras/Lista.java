package taller.estructuras;

import java.util.Iterator;

public class Lista<K, V extends Comparable<V>> implements Iterable {
	private int n;           // number of key-value pairs
	private Node first;      // the linked list of key-value pairs
	private K key;

	// a helper linked list data type
	private class Node {
		private K key;
		private V val;
		private Node next;

		public Node(K key, V val, Node next)  {
			this.key  = key;
			this.val  = val;
			this.next = next;
		}
	}

	/**
	 * Initializes an empty symbol table.
	 */
	public Lista(K pKey) 
	{
		key=pKey;
	}

	/**
	 * Returns the number of key-value pairs in this symbol table.
	 * @return the number of key-value pairs in this symbol table
	 */
	public int size() {
		return n;
	}

	/**
	 * Is this symbol table empty?
	 * @return {@code true} if this symbol table is empty and {@code false} otherwise
	 */
	public boolean isEmpty() {
		return size() == 0;
	}

	/**
	 * Does this symbol table contain the given key?
	 * @param key the key
	 * @return {@code true} if this symbol table contains {@code key} and
	 *     {@code false} otherwise
	 */
	public boolean contains(K key) {
		return this.key.equals(key);
	}

	public K darLlave()
	{
		return key;
	}

	/**
	 * Inserts the key-value pair into the symbol table, overwriting the old value
	 * with the new value if the key is already in the symbol table.
	 * If the value is {@code null}, this effectively deletes the key from the symbol table.
	 * @param key the key
	 * @param val the value
	 */
	public void put(K pKey, V val) {
		Node x = first;

		if(x==null)
		{
			first= new Node(pKey,val,null);
			n++;
			return;
		}

		if(val.compareTo(x.val)<0)
		{
			Node act=new Node(pKey,val,first);
			first=act;
			n++;
			return;
		}

		boolean inserted=false;

		while (x.next != null && !inserted)
		{
			if(x.next.val.compareTo(val)>0)
			{
				Node act=new Node(key, val, x.next);
				x.next=act;
				n++;
				return;
			}
			x = x.next;
		}
		x.next= new Node(key,val,null);
		n++;
	}

	private class Iterador implements Iterator {

		// -----------------------------------------------------------------
		// Atributos
		// -----------------------------------------------------------------

		/**
		 * Nodo actual de la iteración.
		 */
		private Node actual;

		// -----------------------------------------------------------------
		// Constructor
		// -----------------------------------------------------------------
		/**
		 * Crea un nuevo iterador. <br>
		 * <b> post: </b> El iterador se creó con su primer elemento siendo el principio de la lista
		 * @param pData principio, el primer nodo de la lista a iterar.
		 */
		public Iterador(Node principio) {
			actual = principio;
		}

		// -----------------------------------------------------------------
		// Métodos
		// -----------------------------------------------------------------
		/**
		 * Retorna si hay un siguiente elemento para recorrer.
		 * @return true si el nodo siguiente no es nulo, false de lo contrario.
		 */
		public boolean hasNext() {
			boolean rta = true;
			if (actual == null) {
				rta = false;
			}
			return rta;
		}

		/**
		 * Retorna el nodo siguiente de la iteración.
		 * @return Nodo siguiente de la iteración.
		 */
		public V next() {
			V data = actual.val;
			actual = actual.next;
			return data;
		}

	}

	public Iterator<V> iterator() {
		return new Iterador(first);
	}

}