package taller.test;

import java.util.Comparator;
import java.util.Iterator;

import junit.framework.TestCase;
import taller.estructuras.Lista;
import taller.estructuras.TablaLlavesRepetidas;
import taller.mundo.Ciudadano;

public class TablaLlavesRepetidasTest extends TestCase {
	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	 * Clase donde se harán las pruebas.
	 */
	private TablaLlavesRepetidas<String, Ciudadano> tabla1;

	// -----------------------------------------------------------------
	// Métodos
	// -----------------------------------------------------------------

	/**
	 * Escenario 1: Construye una nueva tabla con llaves de Strings, capacidad 5 y factor de carga máxima 0.5
	 */
	private void setupEscenario1() 
	{
		tabla1 = new TablaLlavesRepetidas<String, Ciudadano>(5, 0.5f);
	}

	/**
	 * Prueba 1: Se encarga de verificar el método agregar.<br>
	 * <b> Métodos a probar:</b> <br>
	 * put<br>
	 * get<br>
	 * <b> Casos de prueba: </b><br>
	 * 1. La tabla está vacía. <br>
	 * 2. Se agregan elementos
	 */
	public void testAgregar() {
		setupEscenario1();
		assertEquals("No debería encontrar nada", null , tabla1.get("b"));
		tabla1.put("a", new Ciudadano("a", "b", 1,0,0));
		tabla1.put("c", new Ciudadano("c", "d", 2,0,0));
		assertEquals("La cantidad de elementos debería ser 2", 2, tabla1.darNumElementos());
		assertEquals("La ocupación debería ser 2", 2, tabla1.darOcupacion());
		assertNotNull("debería encontrar el objeto", tabla1.get("a"));
		tabla1.put("a", new Ciudadano("a", "f", 3,0,0));
		tabla1.put("a", new Ciudadano("a", "g", 4,0,0));
		assertEquals("La cantidad de elementos debería ser 4", 4, tabla1.darNumElementos());
		assertEquals("La ocupación debería ser 2", 2, tabla1.darOcupacion());		
		
		tabla1.put("b", new Ciudadano("b", "j", 5,0,0));
		tabla1.put("d", new Ciudadano("d", "k", 6,0,0));
		assertEquals("La cantidad de elementos debería ser 6", 6, tabla1.darNumElementos());
		assertEquals("La ocupación debería ser 4", 4, tabla1.darOcupacion());	
		assertEquals("La capacidad debería haberse duplicado", 10, tabla1.darCapacidad());	
		assertNotNull("debería encontrar el objeto", tabla1.get("b"));
		assertNotNull("debería encontrar el objeto", tabla1.get("d"));
		assertEquals("debería haber 3 elementos con a", 3, tabla1.get("a").size());
	}

	/**
	 * Prueba 1: Se encarga de verificar el método delete de la clase.<br>
	 * <b> Métodos a probar:</b> <br>
	 * delete<br>
	 * <b> Casos de prueba: </b><br>
	 * 1. Se agregan y se quitan elementos.
	 */
	public void testEliminar() 
	{
		setupEscenario1();
		tabla1.put("a", new Ciudadano("a", "b", 1,0,0));
		tabla1.put("a", new Ciudadano("a", "b", 1,0,0));
		tabla1.put("b", new Ciudadano("b", "f", 3,0,0));
		tabla1.put("c", new Ciudadano("c", "h", 4,0,0));
		tabla1.put("c", new Ciudadano("c", "j", 5,0,0));
		tabla1.put("c", new Ciudadano("c", "l", 6,0,0));
		assertEquals("La capacidad debería ser 10", 10, tabla1.darCapacidad());
		assertEquals("El numero de elementos debería ser 6", 6, tabla1.darNumElementos());
		assertEquals("La ocupación debería ser 3", 3, tabla1.darOcupacion());
		assertEquals("El tamaño de la lista con a debería ser 2",2, tabla1.delete("a").size());
		assertNull("No debería encontrar el objeto", tabla1.delete("a"));
		assertEquals("El numero de elementos debería disminuir", 4, tabla1.darNumElementos());
		assertEquals("La ocupación debería ser 2", 2, tabla1.darOcupacion());
		assertEquals("El tamaño de los elementos con c debería ser 3",3,tabla1.delete("c").size());
		assertEquals("La capacidad debería disminuir a 5",5, tabla1.darCapacidad());
		
		assertNull("No debería encontrar el objeto", tabla1.delete("a"));
		assertEquals("El numero de elementos debería disminuir", 1, tabla1.darNumElementos());
		assertEquals("La ocupación debería ser 1", 1, tabla1.darOcupacion());
	}
	
	public void testOrden()
	{
		setupEscenario1();
		tabla1.put("c", new Ciudadano("c", "b", 1,0,0));
		tabla1.put("c", new Ciudadano("c", "a", 1,0,0));
		tabla1.put("b", new Ciudadano("b", "f", 3,0,0));
		tabla1.put("c", new Ciudadano("c", "h", 4,0,0));
		tabla1.put("c", new Ciudadano("c", "z", 6,0,0));
		tabla1.put("c", new Ciudadano("c", "j", 5,0,0));
		tabla1.put("b", new Ciudadano("b", "l", 3,0,0));
		tabla1.put("c", new Ciudadano("c", "l", 6,0,0));
		tabla1.put("c", new Ciudadano("c", "e", 6,0,0));
		
		Lista act= tabla1.get("c");
		Iterator<Ciudadano> i= act.iterator();
		while(i.hasNext())
		{
			System.out.println(i.next().darApellido());
		}
	}
}