package taller.mundo;

public class UbicacionGeografica implements Comparable<UbicacionGeografica>
{
 
	private float latitud;
	
	private float longitud;
	
	public UbicacionGeografica(float f, float g)
	{
		latitud=f;
		longitud=g;
	}
	
	public float darLatitud()
	{
		return latitud;
	}
	
	public float darLongitud()
	{
		return longitud;
	}


	@Override
	public int compareTo(UbicacionGeografica ug1) {
		if(this.latitud-ug1.latitud==0)
		{
			if(this.longitud-ug1.longitud>0)
				return 1;
			return -1;
		}
		if(this.latitud-ug1.latitud>0)
			return 1;
		return -1;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UbicacionGeografica other = (UbicacionGeografica) obj;
		if (latitud != other.latitud)
			return false;
		if (longitud != other.longitud)
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(latitud);
		result = prime * result + Float.floatToIntBits(longitud);
		return result;
	}
	
}
