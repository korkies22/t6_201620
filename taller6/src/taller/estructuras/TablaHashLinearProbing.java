package taller.estructuras;


public class TablaHashLinearProbing<K extends Comparable<K> ,V> {

	//TODO Una enumeracioón que representa los tipos de colisiones que se pueden manejar
	public enum Colisiones
	{
		LINEAR_PROBING,
		SEPARATE_CHAINING
	}

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	//TODO: Agruegue la estructura que va a soportar la tabla.
	private K[] keys;      // the keys
	private V[] vals;    // the values

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	//Constructores

	@SuppressWarnings("unchecked")
	public TablaHashLinearProbing(){
		//TODO: Inicialice la tabla con los valores que considere prudentes para una ejecución normal
		this(4000037,0.5f );
	}

	@SuppressWarnings("unchecked")
	public TablaHashLinearProbing(int pCapacidad, float pFactorCargaMax) {
		//TODO: Inicialice la tabla con los valores dados por parametro
		capacidad= pCapacidad;
		factorCargaMax=pFactorCargaMax;
		factorCarga=0;
		vals = (V[]) new Object[capacidad];
		keys = (K[]) new Comparable[capacidad];
	}

	public void put(K llave, V valor){
		//TODO: Gaurde el objeto valor dado por parametro el cual tiene la llave,
		// double table size if 50% full
		if (factorCarga > factorCargaMax) resize(2*capacidad);
		int i;
		for (i = hash(llave); keys[i] != null; i = (i + 1) % capacidad) {
			if (keys[i].equals(llave)) {
				vals[i] = valor;
				return;
			}
		}
		keys[i] = llave;
		vals[i] = valor;
		count++;
		factorCarga=(float)count/capacidad;
	}

	public int darCapacidad()
	{
		return capacidad;
	}

	public int darNumElementos()
	{
		return count;
	}

	public V get(K llave){
		//TODO: Busque y retorne el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		for (int i = hash(llave); keys[i] != null; i = (i + 1) % capacidad)
			if (keys[i].equals(llave))
				return vals[i];
		return null;
	}

	public V delete(K llave){
		//TODO: borra el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		V retorno= null;
		if ( get(llave) == null) return null;

		// find position i of key
		int i = hash(llave);
		while (!llave.equals(keys[i])) {
			i = (i + 1) % capacidad;
		}

		// delete key and associated value
		keys[i] = null;
		retorno= vals[i];
		vals[i] = null;

		// rehash all keys in same cluster
		i = (i + 1) % capacidad;
		while (keys[i] != null) {
			// delete keys[i] an vals[i] and reinsert
			K   keyToRehash = keys[i];
			V valToRehash = vals[i];
			keys[i] = null;
			vals[i] = null;
			count--;
			put(keyToRehash, valToRehash);
			i = (i + 1) % capacidad;
		}

		count--;
		if (count > 0 && count <= capacidad/8) resize(capacidad/2);
		return retorno;
	}

	//Hash
	private int hash(K llave)
	{
		return Math.abs(((llave.hashCode()/11)-2)%capacidad);
	}

	//TODO: Permita que la tabla sea dinamica
	private void resize(int pCcapacidad)
	{
		TablaHashLinearProbing<K, V> temp = new TablaHashLinearProbing<K, V>(pCcapacidad, factorCargaMax);
		for (int i = 0; i < capacidad; i++) {
			if (keys[i] != null) {
				temp.put(keys[i], vals[i]);
			}
		}
		keys = temp.keys;
		vals = temp.vals;
		capacidad  = temp.capacidad;
	}

}