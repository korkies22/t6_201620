1. La doble generalidad se da porque la clase Nodo puede servir para una tabla con Linear Probing, al incluir listas gen�ricas y por tanto nodos gen�ricos
Mientras que la otra puede implementar separate chaining ya que solo necesita del arreglo sin nodos.

Parte 1
Punto 7
2. Si la llave ya existe en la tabla de hash, sobreescribe el archivo. Si la llave no es la m�sma pero hace colisi�n, se va iterando sobre la tabla de a 1 en 1 hasta encontrar
un espacio vac�o
3.La tabla crece si el factor de carga (n�mero de elementos sobre capacidad) excede cierto l�mite el cual por defecto es 0.5
4. Como funci�n de hash escog� el hashCode que viene con Java, ya que funciona para todos los objetos, la divido en 11 que es un n�mero primo, le resto 2 y 
le saco el valor absoluto. A esto le hago m�dulo la capacidad de la tabla para que al iniciar se dirija a una casilla existente. Todo esto para evitar al m�ximo las colisiones.

Punto 8
5. Al agregar m�s datos el tiempo no cambia significativamente, aumenta un poco el tiempo de agregar para valores altos, pero este tiempo es muy poco.
El tiempo de buscar es casi insignificante, a veces tiende a 0. Por lo que se puede concluir que no depende significativamente del n�mero de datos que se utilice.

Parte 2
Punto 1
6.S� tiene sentido, para mi implementaci�n utilic� algo parecido a una tabla de hash con linear probing, en la que cada casilla guarda una llave en espec�fico como una lista de objetos.
Esto es, al llegar un objeto se intenta ubicar donde haya un espacio vac�o o una lista con su misma llave avanzando con linear probing. 
Si sucede el segundo caso, se a�ade en orden a la lista para cumplir con el requerimiento de dar los datos de manera ordenada.

7. Las tablas de hash no son buena idea cuando se necesita optimizar el espacio en memoria. Ya que hace uso de arreglos con tama�os mucho mayores a la de los datos a guardar.
Tampoco es buena idea si se van a guardar objetos por llaves que puedan repetirse, porque habr�a que hacerle diversas modificaciones a la estructura para cumplir con estos requerimientos.
No es una buena idea si se quieren guardar o retornar elementos de manera ordenada, o que puedan ordenarse de alguna manera. As� m�smo, tampoco es buena idea si se necesita
tener en cuenta el orden de llegada de los elementos.
No es una buena idea si se necesita iterar sobre elementos, nada de lo anteriormente mencionado se puede realizar de manera simple con la implementaci�n actual.